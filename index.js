// 1. bikin class Vehicle (Kendaraan)
// attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi, 
// ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';
class Vehicle {
    constructor (wheel,country){
        this.wheel =  wheel;
        this.country = country
    }
    info(){
        console.log(`Jenis kendaraan ini memiliki roda ${this.wheel} dan diproduksi dari negara ${this.country}`);
    }
}
// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak
class Car extends Vehicle {
    constructor (wheel,country,brand,price,taxPercent){
        super(wheel,country);
        this.brand = brand;
        this.price = price;
        this.taxPercent = taxPercent;
    }

// 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
    totalPrice(){
        let total = this.price + (this.price * (this.taxPercent / 100));
        return total;
    }
// 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
// print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'
    info(){
        super.info();
        console.log(`kendaraan ini bermerek ${this.brand} dengan total harga Rp.${this.totalPrice()}`)
    }
}
// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil)) 
const type = new Vehicle(2 , "China");
type.info();
console.log("\n");
const carType = new Car (4, "Jepang", "Lexus", 3000000000, 10)
carType.info()


// contoh instance
// 1. const kendaraan = new Vehicle(2, 'Jepang')
// kendaraan.info()
// 2. const mobil = new Mobil(4, 'Jerman', 'Mercedes', 800000000, 10);
// mobil.info()

/** 
    NOTES
    rumus total price setelah di tambah pajak utk method totalPrice, bisa kalian googling sendiri yah.
*/
